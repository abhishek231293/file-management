const express = require("express");
const app = express();

const bodyParser = require("body-parser");
const cors = require('cors');
const errorHandler = require('./app/_middleware/error-handler');

//For Response Header Security
const helmet = require("helmet");
app.use(helmet());
app.use(function (req, res, next) {
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({origin: "*"}));

const allowedMethods = [ 'GET', 'POST']
app.use((req, res, next) => {
    if (!allowedMethods.includes(req.method)) {
        let jsonData = {
            "code": 400,
            "failed": "Method not Allowed"
        }
        return res.send({"EncryptedResponse": jsonData })
    }
    return next()
})

require("./app/routes/authRoute.js")(app);
require("./app/routes/filmRoute.js")(app);
require('./app/models/mongoose.js')
app.use(errorHandler);

const PORT = process.env.PORT || 7000;

console.log(PORT);
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
