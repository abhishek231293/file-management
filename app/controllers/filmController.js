const response = require("../_helpers/response")
const FilmService = require('../services/filmService')
const bcrypt = require('bcryptjs');

class FilmController {
    static createNewFilm = async (req, res) => {
        try {
            
            console.log(req.body);
            const newFile = await FilmService.createNewFilm(req.body);

            if(newFile){
                return response(res, 'New film created successfully', 201, newFile, [])
            } else {
                return response(res, 'Unable to create new film', 404, [], []);
            }
            
        } catch(error) {
            let errorsList = [];
            console.log(error);
            if(error.errors){
                for (const key in error.errors) {
                    errorsList.push({error_key: key, error_message: error.errors[key].message})
                }
            }
            return response(res, 'Opps! Something went wrong', 500, [], errorsList)
        }
    }

    static getFilms = async (req, res) => {

        try {
            
            const films = await FilmService.getFilms(req.body.userid)
            return response(res, 'Film fetched successfully', 200, films)

        } catch(error) {
            console.log(error);
            return response(res, 'Opps! Something went wrong', 500, error)
        }

    }

    static getFilm = async (req, res) => {

        try {
            
            console.log(req.params)

            const filmDetail = await FilmService.getFilm(req.params.slug, req.body.userid)
            return response(res, 
                            (filmDetail?'Film Details fetched successfully': 'Film not found'), 
                            (filmDetail?200:404), filmDetail?filmDetail:[])

        } catch(error) {
            console.log(error);
            return response(res, 'Opps! Something went wrong', 500, error)
        }

    }

    static commentFilm = async (req, res) => {

        try {
            
            const comment = await FilmService.commentFilm(req.body)
            return response(res, 'Comment successful!', 200, comment)

        } catch(error) {
            console.log(error);
            return response(res, 'Opps! Something went wrong', 500, error)
        }

    }

}

module.exports = FilmController