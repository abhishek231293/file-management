const response = require("../_helpers/response")
const jwt = require('jsonwebtoken');
const UserService = require('../services/userService')
const bcrypt = require('bcryptjs');

class AuthController {
    static createNewUser = async (req, res) => {
        try{
            const { email_id, mobile_number } = req.body
            
            const existingUser = await UserService.findUser(req.body)

            if(existingUser) {

                const errors = [];
                if(existingUser.email_id == email_id){
                    errors.push({
                        error_key: "email_id",
                        error_message: "Email id already exist!"
                    })
                }

                if(existingUser.mobile_number == mobile_number){
                    errors.push({
                        error_key: "mobile_number",
                        error_message: "Mobile number already exist!"
                    })
                }

                return response(res, 'User Already Exist!', 500, [], errors)
            } else {                
                const newUser = await UserService.createNewUser(req.body)
                if(newUser){
                    return response(res, 'New User Created Successfully', 201, newUser)
                } else {
                    return response(res, 'Failed', 404, [], [{
                        error_key: null,
                        error_message: 'Unable to create a new user'
                    }])
                }
            }

        }catch(error) {
            let errorsList = [];
            console.log(error);
            if(error.errors){
                for (const key in error.errors) {
                    errorsList.push({error_key: key, error_message: error.errors[key].message})
                }
            }
            return response(res, 'Opps! Something went wrong', 500, [], errorsList)
        }
    }

    static loginUser = async (req, res) => {
        const { username, password } = req.body

        const existingUser = await UserService.validateUser(username)

            if(existingUser && bcrypt.compareSync(password, existingUser.password)) {

                delete existingUser["password"];

                
                const token = jwt.sign({ id: existingUser._id, name: existingUser.fullname }, process.env.JWT_SECRET, { expiresIn: '7d' });
                return response(res, 'Login Successful!', 200, [{userDetail: existingUser, token}], [])
            } else {                
                return response(res, 'Invalid Credentials!', 404, [])
                
            }
    }
}


module.exports = AuthController