const CryptoJS = require('crypto-js')
require('dotenv').config()

class AES {
    static AES_KEY = process.env.AES_SECRET
    
    static decryptText(encryptedText) {
        const key = CryptoJS.enc.Utf8.parse(this.AES_KEY);
        const iv = CryptoJS.enc.Utf8.parse(this.AES_KEY);
        const plainText = CryptoJS.AES.decrypt(
            encryptedText,
            key, {
                    keySize: 128,
                    blockSize: 128,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                }
            ).toString(CryptoJS.enc.Utf8);
        return plainText;
    }
}

module.exports = AES
