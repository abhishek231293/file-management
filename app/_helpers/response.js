const response = (res, message, code, data=[], errors = []) => {
    res.send(
        {
            status_code: code,
            message: message,
            data: data,
            errors: errors
        }
    );
}
module.exports = response;
