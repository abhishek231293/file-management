const bcrypt = require('bcryptjs');
const UserModel = require('../models/users.model');
const AES = require('../_helpers/AES')

class UserService {

    static createNewUser = async (userDetail) => {

        // const plainPassword = await AES.decryptText(userDetail.password);
        const plainPassword = userDetail.password;
        const hashPassword = bcrypt.hashSync(plainPassword, 10);
        const newUser = new UserModel();
        newUser.fullname = userDetail.fullname;
        newUser.email_id = userDetail.email_id;
        newUser.mobile_number = userDetail.mobile_number;
        newUser.password = hashPassword;
        await newUser.save();

        return newUser;
    }

    static findUser = async (userDetail) => {

        return await UserModel.findOne({ 
            $or: [ { email_id: userDetail.email_id }, { mobile_number: userDetail.mobile_number } ] 
        })

    }

    static validateUser = async (username) => {

        return await UserModel.findOne({ 
            $or: [ { email_id: username }, { mobile_number: username } ] 
        })

    }

}

module.exports = UserService;