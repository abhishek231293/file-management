const FilmModel = require('../models/films.model');

class FilmService {

   static createNewFilm = async (filmDetail) => {

        const newFilm = new FilmModel();
        newFilm.name = filmDetail.name;
        newFilm.description = filmDetail.description;
        newFilm.release_date = filmDetail.release_date;
        newFilm.ticket_price = filmDetail.ticket_price;
        newFilm.country = filmDetail.country;
        newFilm.genre = filmDetail.genre;
        newFilm.photo = filmDetail.photo;
        newFilm.userid = filmDetail.userid;
        newFilm.slug = filmDetail.name.split(' ').join('_').toLowerCase();

        await newFilm.save();

        return newFilm;

   }

   static getFilms = async (userid) => {
        const films = await FilmModel.find({
            userid
        })
        return films;
   }

   static getFilm = async (slug, userid) => {
        const film = await FilmModel.findOne({
            slug,
            userid
        })
        return film;
   }

   static commentFilm = async (commentDetails) => {
          const newComment = FilmModel.findOneAndUpdate({
               _id: commentDetails.film_id
          },
          { $push: { comments: {
               name: commentDetails.username,
               userid: commentDetails.userid,
               comment: commentDetails.comment
          }  } })

          return newComment;
   }

}

module.exports = FilmService;