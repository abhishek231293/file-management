const AuthController = require('../controllers/authController')

module.exports = (app) => {
    app.post('/auth/create-new-user', AuthController.createNewUser)
    app.post('/auth/login-user', AuthController.loginUser)    
}
