const FilmController = require('../controllers/filmController')
const auth = require('../_middleware/authMiddleware');

module.exports = (app) => {
    app.post('/film/create-new-film', auth, FilmController.createNewFilm)
    app.get('/film/get-films', auth, FilmController.getFilms)    
    app.get('/film/get-film/:slug', auth, FilmController.getFilm)    
    // app.post('/film/rate-film', auth, FilmController.rateFilm)    
    app.post('/film/comment-film', auth, FilmController.commentFilm)    
}
