const jwt = require('jsonwebtoken');
const { LEGAL_TCP_SOCKET_OPTIONS } = require('mongodb');
const response = require('../_helpers/response')
require('dotenv').config()

const auth = async (req, res, next)=>{
    try{

        console.log(' -- MIDDLEWARE -- ')
        let token = req.header('Authorization');
        console.log(token);
        console.log(process.env.JWT_SECRET);

        if(!token){
            return response(res, 'Unauthorized Access', 401)
        }

        const decoded = jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET)
        console.log(decoded);

        if(!decoded) {
            return response(res, 'Token Expire', 401)
        }

        req.body.userid = decoded.id
        req.body.username = decoded.name
        next()

    }catch(error){

        console.log(error);
        response(res, 'Invalid Token', 401)
    }
}

module.exports = auth;
