const mongoose = require('mongoose')
require('dotenv').config()

const UserSchema = new mongoose.Schema({
    fullname: {
        type: String,
        required: [true, 'What is your name?'],
        trim: true,
        lowercase: true
    },
    email_id: {
        type: String,
        required: [true, 'Please provide your email id!'],
        index: true
    },
    mobile_number: {
        type: String,
        required: [true, 'Please provide your mobile number!'],
        minLength: [10, 'Min length of mobile number should be min 10, got {VALUE}'],
        maxLength: [12, 'Max length of mobile number should be max 12, got {VALUE}'],
        index: true
    },
    password: {
        type: String,
        required: [true, 'Please provide the password!'],
        minLength: [10, 'Min length should be 10'],
    },
    is_active: {
        type: Boolean,
        required: true,
        default: true
    }
}, {
    timestamps:true,
    autoIndexId: true 
})

const UserModel = mongoose.model('users', UserSchema)

module.exports = UserModel;
