const mongoose = require('mongoose')
require('dotenv').config()

const FilmSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please provide movie name?'],
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Please provide movie description!'],
        minLength: [50, 'Min length of description should be min 50'],
        maxLength: [1000, 'Max length of description should be max 1000'],
        trim: true
    },
    release_date: {
        type: Date,
        required: [true, 'Please provide release date!']
    },
    ticket_price: {
        type: Number,
        required: [true, 'Please provide ticket price!']
    },
    country: {
        type: String,
        required: [true, 'Please provide country name!']
    },
    genre: {
        type: Array,
        required: [true, 'Please provide genre list!']
    },
    photo: {
        type: String,
        required: [true, 'Please provide movie banner!']
    },
    slug: {
        type: String,
        index: true
    },
    userid: {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        required: [true, 'You are not authorized!']
    },
    comments: {
        type: Array
    },
    user_ratings: {
        type: Array
    },
    avg_rating: {
        type: String
    },
    total_rating: {
        type: Number
    }
}, {
    timestamps:true,
    autoIndexId: true 
})

const FilmModel = mongoose.model('films', FilmSchema)

module.exports = FilmModel;
