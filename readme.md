To setup backend take a clone of this repo and follow below steps:

1. cd /file-management
2. cd backend
3. npm install
4. create a file .env and add below details
    MONGO_DB_URL=<mongodb Connection link>
    PORT=7000
    AES_SECRET=<16 character aes key>
    JWT_SECRET=<64 character alpha number characters JWT Secret Key>
5. nodemon - It will start the backend server no localhost:4000
